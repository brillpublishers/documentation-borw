# documentation-BORW

This gitbook contains information about publishing on BORW, such as

- uploading on dataqa
- uploading on staging/live
- file name conventions
- notifying colleagues
