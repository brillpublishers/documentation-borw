# Klopotek

Klopotek is the name of the database that contains all Brill product data. 

No work can be done on a product unless a Klopotek record has been created. 

It is the responsibility of the acquisitions editor to send a _Create Klopotek Record_ request to the _Product Management_ department (Mireille Pijl, `pijl at brill.com`).
