# Terminology

* **Highwire**.
* **PowerSwitch**. A tool managed by Imre Zevenhuizen to carry out all kinds of different commands on  files in a folder, for example copy to another location.
* **Semantico**. See **Highwire**
* **sftp**. Briefly: sftp stands for "Secure File Transfer Protocol". It is a set of extensions to the HTTP protocol which allows users to collaboratively edit and manage files on remote servers.
* **Webdav**. 
