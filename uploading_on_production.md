# Uploading on production

If, and only if, the content is correct on dataqa, put it in the cold folder for upload to production. The path is `G:\General\Online\Staging Production BORW\2021` followed by a folder for the month. This uploads the content - after 24 hrs and a notification - to the staging of production: http://staging.referenceworks.brillonline.com/subjects

If content is incorrect, notify the platform manager (`Vernooij @ brill.com`) and the content will be removed. Correct teh source files and upload to dataqa again.

If content is correct, notify the platform manager (`Vernooij @ brill.com`) and the content will move to the "live site", https://referenceworks.brillonline.com/subjects

Update the "fragments" - tables of contents and such - and pass them on to the platform manager (Vernooij @ brill.com) for upload to production. Do so well before any deadlines and check them on production. Repeat if necessary.

Notify the product manager (Hoek @ brill.com) well before any deadlines with an overview of new content or other relevant news.

## Loading procedure BORW production ("live site")

1. Upload the XML for a final check on Dataqa (= http://staging.ribo.dataqa.brill.semcs.net/) via `g:\General\Online\Dataqa\Suyver\INPUT - Upload to DATAQA\`
2. If everything is ok, you copy the XML files to e.g. `g:\General\Online\Staging\2016\05-may\lgga\`
3. Daan then uploads the files from there to the Staging environment of the Live site for a final final ceck (= http://staging.referenceworks.brillonline.com)
4. Daan does this a few days before planned publicatino date, so there is (limigted) chance to correct stuff
5. If all goes ok, everything goes live on the first Wednesday of the month

