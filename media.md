# Media

## Upload media files to Dataqa

1. zip all media files into one zip file (do not include a folder name, just the media files) for example `1872-9037_ayb_media.zip`
2. copy the zip file to the folder `g:\General\Online\Dataqa\your-name\INPUT - Upload to DATAQA-Media`
3. within a few minutes, PowerSwitch will take the zip file, remove it and process the files.
4. you will receive an e-mail from PowerSwitch with a list of files updated to the `sftp`. This can take a while, depending on the number and size of the files.
5. The images database of Dataqa is refreshed daily at 13:00 CEST.

## Remarks

If you have more than 3,000 files, the platform manager will need to upload these manually. Add the zip files to `g:\General\Online\Dataqa\Manual-Upload` and send a message to <a href="mailto:support@brill.com">support</a>.

## Upload media files to Staging of Production

The folder for upload onto production: `G:\General\Online\Staging\2021\05-May\acronym\media\`
For Production, the media files do not have to be zipped.

## Images

Daan writes: `figure@entity` must contain the name of the image, not the extension, unless it is an svg

WRONG = `<figure entity="File_1477_Pays-bas_bourguignons-es.jpg" type="figure" pop-up="yes">`

OK = `<figure entity="File_1477_Pays-bas_bourguignons-es" type="figure" pop-up="yes">`

If you add images to RIBO publications, please stick to this file naming convention from now on:

- syntax: SVG, JPG and PDF files: a unique name within the publication media folder:
- characters: `a-z`, `0-9` and `-` and `_`
- NO SPACES ALLOWED
- NO UTF-8 allowed
- PNG is only for cover images
- periods (`.`) may cause difficulties. Please do not use in new publications. It may break the link between the figure@entity and file name. The CRPN  update was delayed because of this. Highwire was not able to find that cause, but the images had a lot of periods in the file name.

Sample tagging in XML:

```xml
<figure entity="File_1477_Pays-bas.svg" type="figure" pop-up="yes"><figdesc><p>caption text</p></figdesc></figure>
```

filename: File_1477_Pays-bas.svg

http://wiki/index.php/Images 


## trouble shooting

Daan says: For image settings, I refer to the article:
 
https://staging-ribo-dataqa-brill.semcs.net/entries/bestencyclopedia/jpg-inline-and-in-frame-not-inline-images_test_article
 
You have two options: Images can be shown inline (figure attribute pop-up="no" ) or in a frame displayed at the right side of the text, figure attribute pop-up="yes", caption text mandatory).
 
For the second, you choose a mix version of tagging, it seems: you added a caption AND figure attribute pop-up="no". If you change that into figure attribute pop-up="yes" the image will be shown in the right column in a small frame.
 
```xml
<p> <figure entity="jpg-testimage-1024" type="figure" pop-up="yes"> <figdesc> <p>jpg-testimage 1024 pixels</p> </figdesc> </figure></p>
```

![](https://data.brill.com/media/daan_plaatje.png)

## images in Brill wiki

Regarding XML and images, here are links to relevant pages in the Brill wiki:

* http://wiki/index.php/XML_structure_Encyclopedia
* http://wiki/index.php/Body_of_the_entry
* http://wiki/index.php/Images
* http://wiki/index.php/Image_Manual
* http://wiki/index.php/Rights_in_pictures

