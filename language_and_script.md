# Language and script

Since RIBO4.3, deployed May 2015, we support three new attributes:

* `@align`
* `@lang`
* `@dir`

@lang and @dir can be used as attributes for text containing elements, such as mainentry, title, foreign, p, ab.

@align, @lang, @dir can be used in paragraph elements: p, ab.

Samples:

```xml
<p lang="cu-Cyrs">text</p>  
<p  dir="rtl" lang="ar-Arab">text</p>
<p  dir="rtl" lang="ar-Arab" align="right">text</p>
<ab  dir="rtl" lang="ar-Arab" align="right">text</ab>

<foreign  dir="rtl" lang="ar-Arab">text</foreign>
```

The CSS will pick up the values after conversion to the online HTML

Known issue: the style sheet has trouble with some nested tagging - and that might be the reason Hebrew and Latin script look to small in DSSELNBT. We are working on that.

XML source sample:

```xml
<p>Just some mixed content Unicode text, using foreign with dir="rtl" and lang="....." set for the respective languages.</p> <p>Baruch 4 (Paralipomena Ieremiae). The work is mentioned in the Index librorum prohibitorum in the so called Pogodin Nomocanon (the earliest copy dating from the 15th century), for which is assumed that it reflected the actual state of South Slavonic apocryphal literature in the 11th–12th century. Slavonic translation is distributed in two versions (CAVT 181–187). The first (i.e. recensio longior vel primitiva) was part of the menaion miscellanies (the oldest contents) and the miscellanies of mixed content, while the second (i.e. recensio retractata integra) was featured in menologia under the date November 4 and has a similar, but nevertheless different Greek prototype. Syriac: <foreign dir="rtl" lang="syr-Syre"> ܪܫܝܡ ܗܘܐ ܒܟܬܒܐ ܝ݂ܘܢܝܐ ܕܡܢܗ ܐܬܦܫܩ ܠܣܘܪܝܝܐ ܟܬܒܐ ܗܢܐ ܕܦܠܐ̈ܬܐ܃ܒܬܪ ܫܘܠܡܐ ܕܝܠܗܝܢ ܗܟܢܐ ܀ ܬܢܼܣ̈ܒܝܢ ܘܐܬܦܚ̈ܡܝܢ ܦܠܐ̈ܬܐ݂܂ ܡܢ ܨܚܚܐ ܚܬܝܬܐ ܕܐܬ݁ܣܝܡ ܘܐܬܟ݂ܬܼܒܘ ܒܗ ܡܢ ܠܒܪ ܣܟ̈ܘܠܝܐ݂܂ ܒܐܼܝܕܐ ܕܦܡܦܝܠܘܣ ܘܕܐܘܣܒܝܘܣ܂ ܕܒܗ ܕ̈ܫܝܡܢ ܗ̈ܘܼܝ ܘܗܠܝܢ ܀ ܐܬܢܼܣ̈ܒܝܢ ܡܢ ܫܬܝܬܝ ܦܨ̈ܐ ܕܐܘܪܓܢܝܣ ܗܠܝܢ ܐܫܟܚܢܢ ܀ ܀ ܀ ܘܬܘܒ ܀ ܒܟܝܪܐ ܕܝܠܗ܂ ܦܡܦܝܠܘܣ ܘܕܐܘܣܒܝܘܣܼ ܬ݁ܪܨܘ ܀ </foreign>Versio slavica, recensio longior vel primitiva (BHG 777; BHGN 77a) is attested fully in the copy: Palimpsestus Sinaiticus: Grech. 70, National Russian Library, Sankt Petersburg (ff. 111v-112v) + Cod. Slav. 34, St. Catherine monastery on Sinai (ff. 30r-40v) + Grech. 70 (ff. 113r-114v), Middle Bulgarian, 13th–14th c. (Sin): <foreign lang="cu-Cyrs">Сло(в) ѥримиѧ пр(о)рка ѡ плѣнѥни града ѥр(с)лмьска. г҃и блг҃ослови ѿче. Бы(с)тъ ѥгда плѣнени бышѫ сн҃ове излеви. цр҃емь хальдеискымъ. гл҃а б҃ь кь ѥримии гл҃ѧ ѥримиѥ избраньниче мои вьставь из[и]ди изь града. сего и ты вароуше занѧ погоуби ѧ. </foreign> </p> <p>The next paragraph is just plain left to right with some Hebrew strings. P-element has no special attributes like dir, lang or align</p> <p>1 Chr 1:12 [foreign dir="rtl" lang="he-Hebr"]<foreign dir="rtl" lang="he-Hebr">מנהון‎</foreign> for [foreign dir="rtl" lang="he-Hebr"] <foreign dir="rtl" lang="he-Hebr">מִשָּׁם‎</foreign> reflects ... [foreign lang="gez-Ethi"] <foreign lang="gez-Ethi"> አዕማዲሁ ገብረ ዘብሩር። ሰርዲኖስ ምስለ ተርሴስ። ኢዮጶሎግዮስ ዘምስለ ሶፎር። ምስማኩ ዘወርቅ። አቍላፊሁ ወአቍፋሊሁ ኢያሴሜር። መንበሩ ዘሜላት። ኅንብርቱ ዘቢረሊዘምስለ ኢያሰጲድ በባሕርይ ክዱን። ውስጡ ንጡፍ በዕብነ ሰንፔር። እፈቅሮ ፊድፋደ እምአዋልደ ኢሩሳሌም። ፃኣ ትርአያ አዋልደ ጽዮን ለንጉሥ ሰሎሞን በአክሊል ዘአስተቀጸለቶ እሙ አመ ዕለተ መርዓሁ ወአመ ዕለተ ፍሥሓ ልቡ። ፃዕ ትርአያ አዋልደ ጽዮን። ለንጉሥ ሰሎሞን። በአክሊል ዘአስተቀጸለቶ እሙ። አመ ዕለተ ሕማሙ። ወአመ ዕለተ ስቅለቱ። ወአመ ዕለተ ሞቱ። አመ ዕለተ መርዓሁ ወአመ ዕለተ ፍሥሕ ልቡ። </foreign> </p>
```

HTML result:
 

This sample comes from: http://staging.ribo.dataqa.brill.semcs.net/entries/bestencyclopedia/script-tagging-simple_article_script_test

Supported scripts:

```
:lang(cu-Cyrs) {
                font-family: Bukyvede; font-size: 95%;
}
/* Old Slavonic should be rendered using the BukyVede fonts. Both language and script are declared. All font sizes for scripts other than Latin, Greek and modern Slavic Cyrillic are set relative to the Brill typeface size. */

:lang(gez-Ethi) {
                font-family: AbyssinicaSIL; font-size: 83%;
}
/* All forms of Ethiopic should be rendered using the AbyssinicaSIL font. At the moment, the THB project only contains Ethiopic text in the Gəʿəz language, hence the language tag. Other Ethiopic language may be added in the future. */

:lang(he-Hebr) {
                font-family: SBLHebrew; font-size: 94%;
}
/* The data does not distinguish between the Hebrew and Aramaic languages through any use of tagging. Hence, (biblical) Aramaic text will also receive the Hebrew language tag, even though it is a different language. This cannot be helped at the moment. */

:lang(syr-Syre) {
                font-family: EstrangeloTalada;
                font-size: 118%;
}
:lang(syr-Syrj) {
                font-family: Serto;
                font-size: 118%;
}
/* At the time of this writing (20 January 2015) Brill has not formally acquired the right to use a webfont version of the Estrangelo Talada font. A request for permission to use web versions of some of the Meltho fonts published by Beth Mardutho [Estrangelo Talada, Serto Batnan, Serto Batnan Bold] was sent to George Kiraz on 7 November 2014 and repeated 16 December 2014. */

:lang(ar-Arab) {
                font-family: Scheherazade; font-size: 150%;
}

/* please note that the specific lines for the next lang - scripts pairs are not finalised */

:lang(arc-Hebr) {
                font-family: SBLHebrew; font-size: 94%;
}

:lang(arc-Armi) {
                font-family: ImperialAramaic; font-size: 100%;
    unicode-bidi: bidi-override;
}

:lang(cop-Copt) {
                font-family: Antinoou; font-size: 90%;
}

:lang(grc-Grek) {
                font-family: Brill; font-size: 100%;
}

:lang(got) {
                font-family: Sadagolthina; font-size: 100%;
}

:lang(hbo-Phnx) {
                font-family: Paleo-Hebrew; font-size: 80%; /* no sample added yet*/
}

:lang(smp-Samr) {
                font-family: Samaritan; font-size: 100%; /* no sample added yet*/
    unicode-bidi: bidi-override;
}


/*RIBO 4.3 Work Order - Addition of support for @lang attribute on certain elements. CSS provided by Pim @ Brill*/

@font-face {
    font-family: 'Brill';
    src:    url('../fonts/BrillRoman.eot') format('embedded-opentype'); /* IE8, IE9  */
    src:    local('☺'),
    url('../fonts/BrillRoman.woff') format('woff'), /* Modern browsers */
    url('../fonts/BrillRoman.ttf') format('truetype');
    font-style: normal;
    font-weight: normal;
    text-rendering: optimizeLegibility;
}

@font-face {
    font-family: 'Brill';
    src:    url('../fonts/BrillItalic.eot') format('embedded-opentype'); /* IE8, IE9  */
    src:    local('☺'),
    url('../fonts/BrillItalic.woff') format('woff'), /* Modern browsers */
    url('../fonts/BrillItalic.ttf') format('truetype');
    font-style: italic;
    font-weight: normal;
    text-rendering: optimizeLegibility;
}

@font-face {
    font-family: 'Brill';
    src:    url('../fonts/BrillBold.eot') format('embedded-opentype'); /* IE8, IE9  */
    src:    local('☺'),
    url('../fonts/BrillBold.woff') format('woff'), /* Modern browsers */
    url('../fonts/BrillBold.ttf') format('truetype');
    font-style: normal;
    font-weight: bold;
    text-rendering: optimizeLegibility;
}

@font-face {
    font-family: 'Brill';
    src:    url('../fonts/BrillBoldItalic.eot') format('embedded-opentype');  /* IE8, IE9  */
    src:    local('☺'),
    url('../fonts/BrillBoldItalic.woff') format('woff'), /* Modern browsers */
    url('../fonts/BrillBoldItalic.ttf') format('truetype');
    font-style: italic;
    font-weight: bold;
    text-rendering: optimizeLegibility;
}

@font-face {
    font-family: 'SBLHebrew';
    src:    url('../fonts/SBL_Hbrw.eot') format('embedded-opentype');  /* IE8, IE9  */
    src:    local('☺'),
    url('../fonts/SBL_Hbrw.woff') format('woff'); /* Modern browsers */
    font-style: normal;
    font-weight: normal;
    text-rendering: optimizeLegibility;
}

@font-face {
    font-family: 'BrillExtra';
    src:    url('../fonts/BrillExtra.eot') format('embedded-opentype');  /* IE8, IE9  */
    src:    local('☺'),
    url('../fonts/BrillExtra.woff') format('woff'), /* Modern browsers */
    url('../fonts/BrillExtra.ttf') format('truetype');
}

@font-face {
    font-family: 'Bukyvede';
    src:    url('../fonts/Bukyvede.eot') format('embedded-opentype');  /* IE8, IE9  */
    src:    local('☺'),
    url('../fonts/Bukyvede.woff') format('woff'); /* Modern Browsers */
    font-style: normal;
    font-weight: normal;
    text-rendering: optimizeLegibility;
}

@font-face {
    font-family: 'Bukyvede';
    src:    url('../fonts/Bukyvede-Italic.eot') format('embedded-opentype');  /* IE8, IE9  */
    src:    local('☺'),
    url('Bukyvede-Italic.woff') format('woff'); /* Modern Browsers */
    font-style: italic;
    font-weight: normal;
    text-rendering: optimizeLegibility;
}

@font-face {
    font-family: 'Bukyvede';
    src:    url('../fonts/Bukyvede-Bold.eot') format('embedded-opentype');  /* IE8, IE9  */
    src:    local('☺'),
    url('Bukyvede-Bold.woff') format('woff'); /* Modern Browsers */
    font-style: italic;
    font-weight: normal;
    text-rendering: optimizeLegibility;
}

@font-face {
    font-family: 'AbyssinicaSIL';
    src:    url('../fonts/AbyssinicaSIL-R.eot') format('embedded-opentype');  /* IE8, IE9  */
    src:    local('☺'),
    url('../fonts/AbyssinicaSIL-R.woff') format('woff'); /* Modern Browsers */
    font-style: normal;
    font-weight: normal;
    text-rendering: optimizeLegibility;
}

@font-face {
    font-family: 'Scheherazade';
    src:    url('../fonts/Scheherazade-R.eot') format('embedded-opentype');  /* IE8, IE9  */
    src:    local('☺'),
    url('../fonts/Scheherazade-R.woff') format('woff'); /* Modern Browsers */
    font-style: normal;
    font-weight: normal;
    text-rendering: optimizeLegibility;
}

@font-face {
    font-family: 'EstrangeloTalada';
    src:    url('../fonts/SyrCOMTalada.eot') format('embedded-opentype');  /* IE8, IE9  */
    src:    local('☺'),
    url('../fonts/SyrCOMTalada.woff') format('woff'); /* Modern Browsers */
    font-style: normal;
    font-weight: normal;
    text-rendering: optimizeLegibility;
}

@font-face {
    font-family: 'ImperialAramaic';
    src: url('../fonts/Aramaic-Imperial-Yeb.eot') format('embedded-opentype');   /* IE8, IE9  */
    src:    local('☺'),
    url('../fonts/Aramaic-Imperial-Yeb.woff') format('woff'); /* Modern Browsers */
    font-style: normal;
    font-weight: normal;
    text-rendering: optimizeLegibility;
}
@font-face {
    font-family: 'Paleo-Hebrew';
    src: url('../fonts/Hebrew-Paleo-Qumran.eot') format('embedded-opentype');   /* IE8, IE9  */
    src:    local('☺'),
    url('../fonts/Hebrew-Paleo-Qumran.woff') format('woff'); /* Modern Browsers */
    font-style: normal;
    font-weight: normal;
    text-rendering: optimizeLegibility;
}
@font-face {
    font-family: 'Samaritan';
    src: url('../fonts/Hebrew-Samaritan.eot') format('embedded-opentype');   /* IE8, IE9  */
    src:    local('☺'),
    url('../fonts/Hebrew-Samaritan.woff') format('woff'); /* Modern Browsers */
    font-style: normal;
    font-weight: normal;
    text-rendering: optimizeLegibility;
}

@font-face {
    font-family: 'Serto';
    src: url('../fonts/SyrCOMBatnan.eot') format('embedded-opentype');   /* IE8, IE9  */
    src:    local('☺'),
    url('../fonts/SyrCOMBatnan.woff') format('woff'); /* Modern Browsers */
    font-style: normal;
    font-weight: normal;
    text-rendering: optimizeLegibility;
}
@font-face {
    font-family: 'Serto';
    src: url('../fonts/SyrCOMBatnanBold.eot') format('embedded-opentype');   /* IE8, IE9  */
    src:    local('☺'),
    url('../fonts/SyrCOMBatnanBold.woff') format('woff'); /* Modern Browsers */
    font-style: normal;
    font-weight: bold;
    text-rendering: optimizeLegibility;
}
```