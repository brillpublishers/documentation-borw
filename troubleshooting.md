# Troubleshooting

**The hotfolder did not eat my zip**

- check the zip: it should _not_ contain any folder
- check the name of the zip file
- check the size of the zip. If it contains more than 3,000 files, send  an e-mail to [support@brill.com],  Subject: RIBO | Manual Upload | Acronym and put the files in `G:\General\Online\Dataqa\Manual Upload`

**The hotfolder ate my zip but my files are not visible on dataqa**

- Check dataqa. Is the product category there? Is the product there? If not, ask <a href="mailto:support@brill.com">support</a> to upload the _title properties file_ and rerun the loader. Make sure the subject of the email is `ADD [product] to [URL]`, e.g. `ADD CATO to https://staging-ribo-dataqa-brill.semcs.net/subjects`.


**I uploaded the wrong files**

- The loader is additive, so adds new stuff to old stuff. To _overwrite_ stuff, reload it. If a file (entry) has the same identifier (ID), it will overwrite the old fiel with the same ID
- If you want to remove content from dataqa, email <a href="mailto:support@brill.com">support</a>
- If you want to remove content from check Page `Other stuff`



