# Brill TEI Epidoc

## Brill TEI Epidoc stuff 1

This is the syntax of an entry URL within a publication on BrillOnline Reference Works: `[static string]/[dynamic string]:`
The static string consists of `[domain name]/entries/[name of publication]/`
The dynamic string consists of two parts: `[part of ID]-[ID]`
The ID here is the value of the `TEI.2@id` attribute in the XML schematized by the Brill TEI Epidoc DTD.

For example:

* URL: http://staging.ribo.dataqa.brill.semcs.net/entries/dead-sea-scrolls-electronic-library-non-biblical-texts/1q14-DSS_EL_NBT_1Q14
* Static string: http://staging.ribo.dataqa.brill.semcs.net/entries/dead-sea-scrolls-electronic-library-non-biblical-texts/
* Dynamic string: 1q14-DSS_EL_NBT_1Q14

Note that in the URL, underscores are used to connect parts of the string. However, in the XML, the DOI, and in the file name, you may encounter hyphens in the same string.

In this example, the file name is `2451-9383_dsselnbt_fulltextxml_DSS-EL-NBT-1Q14.xml` And the XML reads: `<TEI.2 id="DSS-EL-NBT-1Q14">`. DOI: `10.1163/2451-9383_dsselnbt_DSS-EL-NBT-1Q14`

Detail: in a hyperlink (for example `<a href="...">` in HTML, or `TEI.2@rend` in our proprietary XML), you will need the URL, in other words, the string with underscores. However, in the WorkWrapper DTD, you'll need the value as it is in the XML, i.e. the version with hyphens (meaning a hyperlink will look like this: `<section xlink:href="DSS-EL-NBT-1Q14">`).
I don't know how the `[part of ID]` is created. Note it has no upper cases. 

## Brill TEI Epidoc stuff 2

<!-- 
Linking in Brill TEI Epidoc
- Link to e-mail    `<xref email="...">abc@cde.xu</xref>    
- Link to external web page    <xref url="...">link</xref>    This can also be used for linking within the publication, but takes longer.

Note that the url must always be complete (no * etc.).
- Link to entries within the same publication    `<xref target="id of article">`     This is a link to an entry within the same publication.
- Link to entries within any publication on BrillOnline Reference Works    `<link type="acro" targets="id of article">`     This is a link to an entry within the publication with the SAMS acronym "acro" (where acro = acronym of publication).
- Link to sections with entries within a publication on BrillOnline Reference Works    `<link type="acro" targets="id of article" n="id of anchor within article"> `    `n="id of anchor within article"` is optional
-->

You will understand that the xml element <link> is transformed into an html hyperlink (the <a> element). The latter needs a url.
A url can be predicted - if composed of known or predictable elements. Unfortunately it is not possible to link to a section on a page without stating the page (in the form of its web address). In html, you need to know the location as well as the identity.

This, in a nutshell, is the problem to which dereferencable URIs are the solution. Not sure how to implement it on borw though.

## Brill TEI Epidoc stuff 3

If you want a figure online, follow these steps:
1. Is the xml right?
2. Did you load file in media folder?
3. Is the image in the right format?
4. Rodney problem (ask Daan)?

## Linking in Brill TEI Epidoc

  |  |  
----- | ------ | -----
Link to e-mail | `<xref email="...">abc@cde.xu</xref>` | 
Link to external web page | `<xref url="...">link</xref>` | This can also be used for linking within the publication, but takes longer. Note that the url must always be complete (no * etc.).
Link to entries within the same publication | `<xref target="id of article">` | This is a link to an entry within the same publication.
Link to entries within any publication on BrillOnline Reference Works | `<link type="acro" targets="id of article">` | This is a link to an entry within the publication with the SAMS acronym "acro" (where acro = acronym of publication).
Link to sections with entries within a publication on BrillOnline Reference Works | `<link type="acro" targets="id of article" n="id of anchor within article">`| `n="id of anchor within article"` is optional





