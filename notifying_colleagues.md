# Notifying colleagues

Notify _Program Management_ (in the for of Anja van Hoek, `hoek at brill.com`) well in advance of an update. Provide details about which entries are added, or any other significant changes or updates. 

(There used to be a template that I will dig up).
