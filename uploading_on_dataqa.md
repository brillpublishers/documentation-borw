# Uploading on dataqa

## RIBO Dataqa update Process

To update online content on [staging dataqa](http://staging-ribo-dataqa-brill.semcs.net/subjects) you need to copy the files to your designated folder on `g:\` (your "hotfolder"). The platform manager will reload the site daily. In the future, this will be automated as well.

Files supported

* valid xml file per article, file names compliant to standard syntax
* 1 title.properties.xml (already loaded, do not replace, platform manger only)
* 1 toc.xml (special treatment, for now do not replace, platform manger only))
* cover image and other media files (need refresh of RODNEY, scheduled Semantico job)

Important:

* spaces or special characters are not allowed in file names. Use a-z, A-Z, 0-9 and - and _. Media files need to have the extension .jpg (in lower case) or .svg or .pdf. XML files need to have extension xml
* File name convention: `issn-isbn_acronym_file-type_article-ID.ext`. Sample: `1872-9037_ayb_fulltextxml_ayb2013-COM-0053.xml`
* underscores in IDs should be replaced by - in the file name
* For a full description and samples, see `file: G:\Platforms\Brillonline dot com\referenceworks\_procedures\Dataqa00-file-name-conventions-RIBO.docx`

## Procedure

All content of RIBO Dataqa is on a so-called `sftp`. This is secured drive at Highwire.

Content in the so-called hotfolders on `g:\` will be uploaded by PowerSwitch (a tool managed by Imre Zevenhuizen) automatically . After completion, you will receive an e-mail.


## Upload full text xml files to Dataqa

1. zip all full text xml files into one zip file, for example `ayb-update-20210311.zip`, do NOT add a media folder to the zip file. STIPP DOTNET CMS export zip file is OK. _Do not use underscore in the zip file name._ This will confuse Powerswitch.
2. copy the zip file to the folder `g:\General\Online\Dataqa\your-name\INPUT - Upload to DATAQA`
3. within a few minutes, PowerSwitch will take the zip file, remove it and process the files.
4. you will receive an e-mail from PowerSwitch with a list of files updated to the `sftp\data`. This can take a while, depending on the number and size of the files.

## Remarks

1. If you have more than 3,000 files, the platform maanger will need to upload these manually. Add the files to `g:\General\Online\Dataqa\Manual-Upload` and notify <a href="mailto:support@brill.com">support</a>.
2. It is now possible to add a zip file with just one XML file in it.

## How to upload files onto BrillOnline Reference Works?

There are two servers: dataqa, a test environment, and production, the publication environment. Each server has two sides: staging and live. This gives us [four URLs](/URLs.md).

The procedure is that we load files onto the dataqa (or dq for short) to test them before we publish them on production. Loading onto dq means loading onto the staging side (not: site!). Only rarely the live side is used.

Loading the files onto staging dq is done by placing the files in a hot folder. The path is this: `G:\General\Online\Dataqa\your-name` Files must be zipped and have specific names. Details can be found in the documents that are uploaded below. The folder is 'hot' because the files are supposedly moved automatically to Semantic's servers. In practice, the platform manager still has to switch on the file processing on the server.

Semantico's software does two things: index entries to make them searchable, and turn them from XML into HTML). This procedure only works for existing publications. For new ones, contact the platform manager, who will need to create an entry in SAMS (access management database.

Loading onto the staging of the production server is done by placing the files into the appropriate folder in this path: `G:\General\Online\Staging Production BORW` In this case, the files don't need to be zipped.

You can have a last check on the staging, and if all is ok, the platform manger will put the publication 'live'. That is to say: the platform switches sides and the publications that were previously found on staging now has the live url attached to them.

Lastly a general rule: The deadline for final files to go live the first Thursday of next month is the third Wednesday of the month before.

