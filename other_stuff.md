# Other stuff

## WorkWrapper DTD

In the XML files according to the work-wrapper.dtd, the value of `section@xlink:href` is identical to the value of `TEI.2@id` of the entry to which you are linking.

## Use of printers for freelancers

In the Leiden office, use of the printers requires a **pincode**. This works for any printer in the building. By default, the pincode is identical to the **last four digits** of an employees telephone number. (Remember those?)  Colleagues who don't have a telephone number, such as interns and freelancers, but who **do have a Brill account** can acquire a pincode in two ways:

1.    Ask IT (`helpdesk at brill.com`) for a pincode
2.    Create a pincode yourself by following these steps:
  a.    Open BrillNet
  b.    Select under Apps: SafeQ Printer
  c.    Log in with your username / password for the netwerk
  d.    In the dashboard you nee, there the option “Generate PIN”. Use it to create a pincode

## Remove content

If you want to **remove** content from [staging](http://staging.referenceworks.brillonline.com/) or [production](https://referenceworks.brillonline.com/), send an email to [support](email:support@brill.com), Subject: RIBO | Remove | Acronym.

How to request to remove content from https://referenceworks.brillonline.com?
To make this most easy for us is to add the XML file that you want to have removed to
`g:\General\Online\Staging Production BORW\2018\11-Nov\_REMOVE\`
Because ALL XML files are unique, you may enter them all in the same REMOVE folder.

## Upload over 3,000 files
The PowerSwitch tool to upload files to `Dataqa` has problems with zip files that have more than 3,000 files - if you need to replace a complete publication with more than 3,000 files, please do NOT upload the zip file to `g:\General\Online\Dataqa\[your name]\` but send  an e-mail [support@brill.com],  Subject: RIBO | Manual Upload | Acronym and put the files in `G:\General\Online\Dataqa\Manual Upload`

## Date created / date updated

All entries on BORW (= RIBO) are to contain a "date.created" and sometimes a "date.updated".

The platform uses this information 1) in the search facet "sort by date" and 2) in the citation at the footer of each entry.
For the encyclopedia.dtd, these are the attributes @first-online and @last-update. See the Brill Wiki - http://wiki/index.php/XML_structure_Encyclopedia - for a description.

For the Brill TEI-epidoc.dtd. the XML looks like this:
`<teiHeader status="new" type="text" date.created="yyyymmdd" date.updated="yyyymmdd">`

The value for date.created is the print date of the publication (unless it is online only, then add the expected launch date). And the data format is yyyymmdd. (If no day and month are known, enter yyyy1231).

The date is also used in the so-called Dublin Core files that Daan sends to discovery services such as OCLC and EBSCO. (edited) 

## Useful links and paths

* [the Brill wiki](http://wiki/index.php/Main_Page)
* g:\DMDP\Platform Technology\platforms\referenceworks\_procedures\Dataqa\

## ISSN / acronyms

List of permitted issn/acronymen can be found here: g:\General\Online\Dataqa\MRW_issn_acronym_list.xlsx

## Gratis access

Any **gratis access** for any SAMS titles or Ebooks should go to the [program manager](email:hoek@brill.com) to be arranged. There are processes in place to track access for auditory purposes.

Not forgetting to provide the following information.
*  End user first name / last name
*  End user affiliation
*  End user email address
* Product title(s)
* Access time period – an end date is mandatory as we need to make sure data is up to date and accurate. After the expiry date we will check the credentials and access can be extended. If no end date is given access will be set up for 3 months
* Reason for gratis access

If the Gratis access concerns an Organisation or Institution then please remember to contact the relevant Sales Manager so that they are informed.




