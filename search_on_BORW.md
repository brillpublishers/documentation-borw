# Search on BORW

## Full text search

Search on the level of the publication:
http://referenceworks.brillonline.com/search?s.q=pindakaas&s.f.s2_parent=s.f.book.brill-s-new-jacoby&search-go=Search

Search on the level of the cluster:
http://referenceworks.brillonline.com/search?s.q=pindakaas&s.f.s2_parent=s.f.cluster.Jacoby+Online&search-go=Search


Search on the level of the platform:
http://referenceworks.brillonline.com/search?s.q=pindakaas&s.f.s2_parent=&search-go=Search
But is it also possible to search two or more specific publications at the same time? So far, my attempts fails. This, for example, searches only BNJ2 and not BNJ:
http://referenceworks.brillonline.com/search?s.q=pindakaas&s.f.s2_parent=s.f.book.brill-s-new-jacoby&s.f.s2_parent=s.f.book.brill-s-new-jacoby-2&search-go=Search

Wheras this only searches BNJ: http://referenceworks.brillonline.com/search?s.q=pindakaas&s.f.s2_parent=s.f.book.brill-s-new-jacoby&brill-s-new-jacoby-2&search-go=Search

And is it possible to search for keywords on cluster level? This was successful:
http://referenceworks.brillonline.com/search?s.q=tei_sourcedate:%2210th%20century%20AD%22&s.f.s2_parent=s.f.cluster.Jacoby+Online
This is encouraging in the light of the goal to create a common front page for JO.

## Pre-fab search (“blauwtjes”)

Ever clicked on the blue terms in Jacoby Online? They’re “prefab search” and we usually just call them “blauwtjes”.
How does it work? It is a variation on the hyperlink: a given term is blue and clickable. However, it is not a hyperlink to a document, but a link to a page with search results. The results are all documents that contain the same term of the same type. For example:

```xml
<index indexName="ethno-name" display="Scythian" level1="" level2="">Scythian</index>
```

becomes:

http://referenceworks.brillonline.com/search?s.q=tei_ethnoname:%22Scythian%22&s.f.s2_parent=s.f.book.brill-s-new-jacoby

The term here is “Scythian” and the type is “tei_ethnoname”.

Here is a list of all permissible types and the appropriate XML:

* tei_propername: ?
* dc_coverage: ? wtf??
* tei_ancientauthortitle: ?
* tei_ethnoname: `<index indexName="ethno-name">`
* tei_sourcedate: ?
* tei_sourcelanguage: ?
* tei_subject: ?
* tei_place: ?
* ?: ?

Technically speaking, “blauwtjes” are created by the BORW platform when an XSLT renders a given part in the XML – to wit <index> - as a hyperlink (which really is an http GET request to its own server).

Questions
1. Does prefab search work on cluster level? Or for multiple publications?
2. Can blauwtjes be combined in a prefab search? (This would allow for more refined queries).

Ad 1. Note how s.f.s2_parent=s.f.book determines what publication is searched. Therefore, this should work:
http://referenceworks.brillonline.com/search?s.q=tei_ethnoname%3A%22Scythian%22&s.f.s2_parent=s.f.cluster.Jacoby+Online
And it does! So we have cluster blauwtjes. Unfortunately, it is not possible to place this URL back into the XML. Because in HTML, this URL is changed into a slightly different URL as a result of different encodings! So now what?

Ad 2: I can’t get this to work other than with Booleans. For example:
http://referenceworks.brillonline.com/search?s.q=tei_propername%3A%22Attalos+%28II%29%22+AND+tei_propername%3A%22Attalos+%28III%29%22&s.f.s2_parent=s.f.book.brill-s-new-jacoby

Here is a list of all permissible types and the appropriate XML:
* tei_propername: ?
* dc_coverage: ? wtf??
* tei_ancientauthortitle: `<index indexName="ancient-author">`
* tei_ethnoname: `<index indexName="ethno-name">`
* tei_sourcedate: ?
* tei_sourcelanguage: ?
* tei_subject: `<index indexName="subject">`
* tei_place: ?
* tei_historicalwork: `<index indexName="historical-work">`


