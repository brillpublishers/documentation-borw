# Folder structure

Content for publication is stored at Brill at `G:\Publishing Projects` which is the same as `P:\`. 

Publishing units, product types, and publications each have their own folder. LGGA, for example, lives at `G:\Publishing Projects\ARC\CLS\3 Major Works_MRWs\LGGA`.

The product folder tends to have a number of default subfolders:

1. Documents, where we store proposals, DCFs, guidelines and other documentation
2. Contracts, where we store contracts with editors, authors, etc.
3. Workflow, where we store files that we are working on
4. BORW, where we store the content that is live on BORW, i.e. the [BrillOnline Reference Works platform](https://referenceworks.brillonline.com/subjects).

There can be many other folders. LGGa, for example, has a `5. Old` for keeping stuff because "you never know"...

![image of folder structure](/folder-structure.png)

The contents of `4. BORW` is always the latest and greatest: the most recent, most complete, most correct dataset as it is published on the liber site (the "production environment").

A copy of that dataset can be found in our digital archive on `Q:\`. It may take some time for the content to be copied to `Q:\`, though, as it is a manual process.

The contents of `3. Workflow` is work in progress. The challenge is to keep track of all of the changes that the content - sometimes thousands of entries - undergoes. Often, there can be a complex workflow with multiple stakeholders - authors, academic editors, copy editors, translators, typesetters - and multiple feedback loops.

Often, project co-ordinators take a two-fold approach:

1. They keep track of the content status by keeping a spreadsheet in which each row represents an entry and each row repesents a stage in the workflow and/or some other relevant category (like names and addresses, or status)
2. They keep track of the content files by creating a system of folders, where each folder represents a stage in the workflow

