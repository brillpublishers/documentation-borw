# URLs

## overview of working and non-working URLS

DOES work:

site | URL
----- | -----
staging dataqa | https://staging-ribo-dataqa-brill.semcs.net/subjects
dataqa | https://ribo-dataqa-brill.semcs.net/subjects
staging production | http://staging.referenceworks.brillonline.com/subjects
production | https://referenceworks.brillonline.com/subjects 

does NOT work:

site | URL | status
----- | ----- | -----
staging dataqa | http://staging-ribo-dataqa-brill.semcs.net/subjects | redirects to https://staging-ribo-dataqa-brill.semcs.net/subjects
staging dataqa | http://staging.ribo.dataqa.brill.semcs.net/ | DNS points to prohibited IP
staging dataqa | https://staging.ribo.dataqa.brill.semcs.net/ | This site can’t be reached
dataqa | http://ribo.dataqa.brill.semcs.net/ | DNS points to prohibited IP
dataqa | https://ribo.dataqa.brill.semcs.net/ | This site can’t be reached
staging production | https://staging.referenceworks.brillonline.com/subjects | NET::ERR_CERT_AUTHORITY_INVALID
production | http://referenceworks.brillonline.com/subjects | redirects to https://referenceworks.brillonline.com/subjects

<!-- >
terrible mistakes:

* https://www.ssllabs.com/ssltest/analyze.html?d=staging.ribo.dataqa.brill.semcs.net
* https://www.ssllabs.com/ssltest/analyze.html?d=staging.referenceworks.brillonline.com
-->

## https

In September 2018, we switched from `http` to `https`. Please use `https` links in the entry metadata.

For example

```xml
<encyclopedia><div2><head/><art><complexarticle targets="https://referenceworks.brillonline.com/entries/encyclopedia-of-hebrew-language-and-linguistics/*EHLL_SIM_000565" idno.doi="http://dx.doi.org/10.1163/2212-4241_ehll_EHLL_SIM_000565" id="EHLL-SIM-000565" type="" entry="Guttural Consonants: Masoretic Hebrew (Vol 2)" volume="" page="165" first-online="20130624" last-update="" first-print="9789004176423">
```

or

```xml
<TEI.2 rend="https://referenceworks.brillonline.com/entries/international-maritime-boundaries/*IMBO_Book_9_53" ana="http://dx.doi.org/10.1163/2214-8388_rwimbo_IMBO_Book_9_53" id="IMBO-Book-9_53" lang=""><teiHeader date.created="20180928" date.updated="" status="new" type="text" TEIform="teiHeader">
```

## BOBI

**BrillOnline Bibliographies**

DOES work:

site | URL
----- | -----
production | https://bibliographies.brillonline.com/
staging production | http://staging.bibliographies.brillonline.com/
dataqa | ?
staging dataqa | ?

does NOT work:

site | URL | status
----- | ----- | -----
production | http://bibliographies.brillonline.com/ | redirects to https://bibliographies.brillonline.com/
staging production | https://staging.bibliographies.brillonline.com/ | NET::ERR_CERT_AUTHORITY_INVALID
dataqa | http://bibliographies.ribo.dataqa.brill.semcs.net/ | DNS points to prohibited IP
dataqa | https://bibliographies.ribo.dataqa.brill.semcs.net/ | ERR_SSL_VERSION_OR_CIPHER_MISMATCH
staging dataqa | http://staging.bibliographies.ribo.dataqa.brill.semcs.net/ | DNS points to prohibited IP
staging dataqa | https://staging.bibliographies.ribo.dataqa.brill.semcs.net/ | ERR_SSL_VERSION_OR_CIPHER_MISMATCH
