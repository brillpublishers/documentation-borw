# File name convenstions

<!--

## Contents

Folders	1
File name conventions for sftp storage for Online:	2
File name convention for storage in Archive on Q	2
File name convention for Discovery Services	3
File name convention for Vendors	3

Version History
version
date
who
change
0.1
20150616
Daan Vernooij
draft text added
0.2
20210318
Daan Vernooij
text updated to refelct sftp instead of webdav
-->

## Folders

Publications are stored in a folder on sftp/data/import/source_files. The folder name is the acronym of the publication. No spaces allowed.

## File name conventions for sftp storage for Online

6 file types are supported. Each file type has a unique name part and or extension. No spaces allowed.

* allowed types:
  * fulltextxml (one file per article)
  * toc (only 1 file allowed)
  * title.properties (only 1 file allowed)
  * media files: extension (lower case only)
            ▪ jpg
            ▪ pdf
            ▪ svg

* syntax: issn-isbn_acronym_type_ID.ext (but not for media files, these have a unique name within the media folder)
* extensions are case sensitive: use `.jpg`, **not** `.JPG`

* Samples:
  * sftp\data\import\source_files\ayb\1872-9037_ayb_fulltextxml_ayb2013-COM-0053.xml 
  * sftp\data\import\source_files\ayb\1872-9037_ayb_toc_aybtoc.xml 
  * sftp\data\import\source_files\ayb\media\image1.jpg
  * sftp\data\import\source_files\ayb\media\image31.svg
  * sftp\data\import\source_files\ayb\media\textsource.pdf

* underscores in IDs should be replaced by - in the file name

## File name convention for storage in Archive on Q

* allowed types:
  * dublincorefullplaintext
  * fulltextxml
  * toc
  * title.properties
  * media files (to be added semi-automatically using Powerswitch)

* syntax: issn-isbn_acronym_type_ID.ext (but not for media files, these have a unique name within the media folder, all media files must be zipped into one folder)

* Sample:
  * 1872-9037_ayb_fulltextxml_ayb2013-COM-0053.xml 
  * 1872-9037_ayb_toc_aybtoc.xml 
  * media\abd-123.jpg

* underscores in IDs should be replaced by - in the file name

## File name convention for Discovery Services

* allowed type: dublincorefullplaintext
* Sample: 1872-9037_ayb_ dublincorefullplaintext _ayb2013-COM-0053.xml
* Delivery to Discovery Services: zip all dublincorefullplaintext xml files in one zip: issn-isbn_acronym-date.zip
* underscores in IDs should be replaced by - in the file name

## File name convention for Vendors 

File name convention for Vendors is the same as for our online: 6 file types are supported. Each file type has a unique name part or extension. 

* allowed types: 
  * dublincorefullplaintext
  * fulltextxml
  * toc
  * title.properties
  * media files (jpg, pdf, svg)

* syntax: issn-isbn_acronym_type_ID.ext (but not for media files, these have a unique name within the media folder)

* Sample:
  * 1872-9037_ayb_fulltextxml_ayb2013-COM-0053.xml 
  * 1872-9037_ayb_toc_aybtoc.xml 
  * media\image1.jpg
  * media\image31.svg
  * media\textsource.pdf

* underscores in IDs should be replaced by - in the file name

* Delivery to Vendors: zip all files together in one package: issn-isbn_acronym-date.zip

## File names in CMS export

Stipp DOT.NET CMS systems (EMHO, FLG)  already support the correct file name convention. Export as "online" will generate a zip file that can be put in the upload folder for Dataqa right away.

## File naming conventions BrillOnline Reference Works

- each entry is stored in a separate file
- the name of the XML-file is a combination of ISSN, Abbreviation, the string "fulltextxml" and the record ID. This information is combined as follows: `<issn>_<abbreviation>_fulltextxml_<record-id>.xml`
- the abbreviation is the same as set in SAMS. Since 2019, new products use the Klopotek acronym in CAPS.
- a listing of issn and abbreviation is available in `MRW - acronyms`

Examples:

* `2405-447X_loro_fulltextxml_ COM-000203.xml`
* `2212-4241_ehll_fulltextxml_EHLL-COM-00000449.xml`
* `2666-4941_WTCO_fulltextxml_COM-1049.xml`

Note that in the second example the string "EHLL" is also part of the record-id.

In case an underscore is used in the article ID in the XML, the underscore must be replaced with a dash in the filename. So COM_123456 in the XML should become COM-123456 in the filename.

Retrieved from "http://wiki/index.php/File_naming_conventions"

