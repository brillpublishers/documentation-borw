# Webdav

## Folders on Webdav Dataqa 

Publications are stored in a folder on `sftp/data/import/source_files/acronym`. The folder name is the acronym of the publication.

PowerSwitch analyses the name of the files uploaded, and will put them in the proper folder.

### File name conventions for Webdav storage for Online:
 
6 file types are supported. Each file type has a unique name part and or extension. 

* allowed types:
  * fulltextxml (one file per article)
  * toc (only 1 file allowed)
  * title.properties (only 1 file allowed, only for Platform manager)
  * media files: extension:
    * jpg
    * pdf
    * svg
* syntax:	`issn-isbn_acronym_type_ID.ext` (but not for media files, these have a unique name within the media folder)
* Samples:
  * `sftp\data\import\source_files\ayb\1872-9037_ayb_fulltextxml_ayb2013-COM-0053.xml`
  * `sftp\data\import\source_files\ayb\1872-9037_ayb_toc_aybtoc.xml`
  * `sftp\data\import\source_files\ayb\test_testency_fulltextxml_TESTCOM-123.xml`
  * `sftp\data\import\source_files\ayb\media\image1.jpg`
  * `sftp\data\import\source_files\ayb\media\image31.svg`
  * `sftp\data\import\source_files\ayb\media\textsource.pdf`

### File names in CMS export

Stipp DOT.NET CMS systems (EMHO, FLG) already support the correct file name convention. Export as "online" will generate a zip file that can be put in the upload folder for Dataqa right away.

