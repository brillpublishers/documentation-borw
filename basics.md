# Basics

How to upload content to BORW?
[to be added: media]

1. Zip the new content from the content folder. The XML files must be zipped into one file, name it for example: `acro-date-today.zip` Do not add _ to the zip file name, as this will confuse the Powerswitch tool. Make sure the zip file contains at least two xml files.
2. Put the zip file in the _hot folder_ to upload the content to **dataqa**. The path is `g:\General\Online\Dataqa\your-name\INPUT - Upload to DATAQA` You receive a notification of successful - or unsuccessful - upload.
3. Check the content on the staging of dataqa after 24 hrs. Here: https://staging-ribo-dataqa-brill.semcs.net/subjects If content is incorrect, correct the source files and repeat steps 1-3.
4. If, and only if, the content is correct, put it in the _cold folder_ for upload to **production**. The path is `G:\General\Online\Staging Production BORW\2021\` followed by a folder for the month, for example 05-May. Do not zip the files. The platform manager will load the content to Staging at the 3d Friday of the month prior to the monthy update. The Platform Manager will inform you when http://staging.referenceworks.brillonline.com/subjects is ready for approval.
5. If content is correct, notify the platform manager (`Vernooij @ brill.com`) and the content will move to the "live site", https://referenceworks.brillonline.com/subjects first Thursday of the next month.
6. Update the "fragments" on first on Dataqa - tables of contents and such - and pass them on to the support @ brill.com (`support @ brill.com`) for upload to production. Do so well before any deadlines and check them on production. Repeat if necessary. It is not possibe to test Fragments on Staging. They go live the first Thursday of the next month
7. Notify the program manager (`Hoek @ brill.com`) well before any deadlines with an overview of new content or other relevant news.

More in-depth information can be found in the other chapters of this git-book. Don't forget to use the search function.
